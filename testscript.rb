class Testscript < Formula
  desc ""
  homepage ""
  url "https://bitbucket.org/pixelfarm_erwin/testscript/get/v1.0.5.tar.gz"
  sha256 "ae58e39e9768cdaef69d1f9d4aa32db591ef4a7a08e1c69cba0472c3845a2211"

  def install
	bin.install "testscript"
  end
 end